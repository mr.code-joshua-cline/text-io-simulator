/**
 * This is the LIVE textIOWrapper.
 * It will handle the actual data IO.
 * It is still in progress
 * It should be included in-line with any sent files.
 * @class
 * @in-progress
 */
class textIOWrapper{
  constructor(){
    /**
     * How many lines have been read.
     * @public
     * @type {number}
     * @readonly
     */
    this.linesRead = 0;
    /**
     * How many lines have been sent.
     * @public
     * @type {number}
     * @readonly
     */
    this.__linesSent = 0;
    /**
     * Options for the wrapper
     * @public
     * @type {Object}
     * @prop {number} marginOfError The acceptable margin of error. (Will still display warnings)
     * @prop {boolean} isDevelopment If the current wrapper is development or live.
     */
    this.options = options || {};
    this.options.marginOfError || 0;
    this.options.isDevelopment = false;
  }
  /**
   * Gets a line of input.
   * @return {String} The line of input.
   */
  getLine(){

  }
  /**
   * Prints a line of output.
   * @param {String} outputString - A line of output.
   */
  sendLine(outputString){

  }
}
